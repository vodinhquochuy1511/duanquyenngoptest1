<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_FILE_ADMIN')) {
    exit('Stop!!!');
}

include 'project.php';

$page_title = $lang_module['duankhongduyet'];

$sql = "SELECT nv4_vi_duanquyengop_themmoiduans.* ,nv4_users.first_name 
        FROM nv4_vi_duanquyengop_themmoiduans 
        LEFT JOIN nv4_users 
        ON nv4_vi_duanquyengop_themmoiduans.id_nguoi_tao = nv4_users.userid 
        WHERE nv4_vi_duanquyengop_themmoiduans.is_duyet != 2";

$list_du_an = $db->query($sql)->fetchAll();

if($nv_Request->isset_request('chuyenveduan','post,get')){
    $_POST['is_duyet'] = 1;
    $update = update($_POST['id'], $_POST,$table_du_an);
    if($update){
        nv_jsonOutput([
            'status' => true,
        ]);
    }else{
        nv_jsonOutput([
            'status' => false,
        ]);
    }
}

$xtpl = new XTemplate('duankhongduyet.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);

foreach($list_du_an as $key => $value){
    $value['hinh_anh'] = explode(',', $value['hinh_anh'])[0];
    $xtpl->assign('VALUE', $value);
    $xtpl->assign('KEY', $key + 1);
    $xtpl->parse('main.list_du_an');

}

$xtpl->parse('main');
$contents = $xtpl->text('main');

include NV_ROOTDIR . '/includes/header.php';
echo nv_admin_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
