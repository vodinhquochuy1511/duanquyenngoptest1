<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_FILE_ADMIN')) {
    exit('Stop!!!');
}
include 'project.php';

$page_title = $lang_module['danhsachduan'];

$sql = "SELECT nv4_vi_duanquyengop_themmoiduans.* ,nv4_users.first_name 
        FROM nv4_vi_duanquyengop_themmoiduans 
        LEFT JOIN nv4_users 
        ON nv4_vi_duanquyengop_themmoiduans.id_nguoi_tao = nv4_users.userid 
        WHERE nv4_vi_duanquyengop_themmoiduans.is_duyet != 2";

$list_du_an = $db->query($sql)->fetchAll();

if($nv_Request->isset_request('motangan','post,get')){
    $data = check($_POST['id'],'id', $table_du_an);
    // print_r($data);die;
    nv_jsonOutput([
        'data' => $data
    ]);
}

if($nv_Request->isset_request('motachitiet','post,get')){
    $data = check($_POST['id'],'id', $table_du_an);
    nv_jsonOutput([
        'data' => $data
    ]);
}

if($nv_Request->isset_request('delete','post,get')){
    $sql = 'DELETE FROM nv4_vi_duanquyengop_themmoiduans WHERE id=' . $_POST['id'];
    $exe = $db->query($sql);
    if($exe){
        nv_jsonOutput([
            'status' => true,
        ]);
    }
}

if($nv_Request->isset_request('update','post,get')){
    $sql = 'SELECT * FROM `nv4_vi_duanquyengop_themmoiduans` WHERE id=' . $_POST['id'];
    $res = $db->query($sql);
    $data= $res->fetch();
    if($data){
        nv_jsonOutput([
            'data' => $data,
        ]);
    }
}

if($nv_Request->isset_request('duyet','post,get')){
    $sql = 'SELECT * FROM `nv4_vi_duanquyengop_themmoiduans` WHERE id=' . $_POST['id'];
    $res = $db->query($sql);
    $data= $res->fetch();
    if($data){
        nv_jsonOutput([
            'data' => $data,
        ]);
    }
}

if($nv_Request->isset_request('acceptDuyet','post,get')){
    if(empty($_POST['ma_du_an'])){
        nv_jsonOutput([
            'status' => false,
        ]);
    }else{
        $_POST['is_duyet'] = 1;
        $update_ma_du_an = update($_POST['id'], $_POST, $table_du_an);
        if($update_ma_du_an){
            nv_jsonOutput([
                'status' => true,
            ]);
        }else{
            nv_jsonOutput([
                'status' => false,
            ]);
        }
    }
    
    
}

if($nv_Request->isset_request('unduyet','post,get')){
    $_POST['is_duyet'] = 3;
    $update = update($_POST['id'], $_POST, $table_du_an);
    if($update){
        nv_jsonOutput([
            'status' => true,
        ]);
    }else{
        nv_jsonOutput([
            'status' => false,
        ]);
    }
}

if($nv_Request->isset_request('acceptUpdate','post,get')) {
    $link_luu_anh = NV_ROOTDIR . '/' . NV_UPLOADS_DIR . '/' . $module_name;
    $list_anh = '';
    if(empty($_FILES)){
        $_POST['hinh_anh'] = $_POST['link_image'];
    }else {
        for($i = 0; $i < $_POST['so_luong_anh'];$i++){
            $upload = new NukeViet\Files\Upload('images', $global_config['forbid_extensions'], $global_config['forbid_mimes'], NV_UPLOAD_MAX_FILESIZE, NV_MAX_WIDTH, NV_MAX_HEIGHT);
            $upload->setLanguage($lang_global);
            $upload_info = $upload->save_file($_FILES['hinh_anh_'.$i], $link_luu_anh, false, $global_config['nv_auto_resize']);
            if(strlen($list_anh) > 10){
                $list_anh .= ',' . NV_BASE_SITEURL . '/' . NV_UPLOADS_DIR . '/' .$module_name . '/' . $upload_info['basename'];
            }else{
                $list_anh = NV_BASE_SITEURL . '/' . NV_UPLOADS_DIR . '/' .$module_name . '/' . $upload_info['basename'];
            }
        }
        $_POST['hinh_anh'] = $list_anh;
    }
    $_POST['slug_du_an'] = create_slug($_POST['ten_du_an']);
    foreach ($fillable as $key => $value) {
        $post[$value] = $_POST[$value];
    }
    $update = update($_POST['id'], $post, $table_du_an);
    if($update){
        nv_jsonOutput([
            'status' => true,
        ]);
    }else{
        nv_jsonOutput([
            'status' => false,
        ]);
    }
    
}

if($nv_Request->isset_request('khongduyet','post,get')){
    $_POST['is_duyet'] = 2;
    $update = update($_POST['id'], $_POST, $table_du_an);
    if($update){
        nv_jsonOutput([
            'status' => true,
        ]);
    }else{
        nv_jsonOutput([
            'status' => false,
        ]);
    }
}

if($nv_Request->isset_request('delete','post,get')){
    $sql = "DELETE FROM `nv4_vi_duanquyengop_themmoiduans` WHERE id=" . $_POST['id'];
    $res = $db->query($sql);
    if($res){
        nv_jsonOutput([
            'status' => true,
        ]);
    }else{
        nv_jsonOutput([
            'status' => false,
        ]);
    }
}

if($nv_Request->isset_request('change','post,get')){
    $check = check($_POST['id'],'id', $table_du_an);
    $_POST['is_open'] = $check['is_open'] == 1 ? 2 : 1;
    $update = update($_POST['id'], $_POST,$table_du_an);
    if($update){
        nv_jsonOutput([
            'status' => true,
        ]);
    }else{
        nv_jsonOutput([
            'status' => false,
        ]);
    }
}


$xtpl = new XTemplate('danhsachduan.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);

foreach($list_du_an as $key => $value){
    $value['hinh_anh'] = explode(',', $value['hinh_anh'])[0];
    if($value['is_open'] == 1){
        $x = '<button class="change btn btn-primary" data-id="'. $value['id'] .'">Hiển thị</button>';
        $xtpl->assign('IS_OPEN', $x);
    }else{
        $x = '<button class="change btn btn-danger" data-id="'. $value['id'] .'">Tạm tắt</button>';
        $xtpl->assign('IS_OPEN', $x);
    }
    if($value['is_duyet'] == 3){
        $x = '<button class="duyet btn btn-success" data-id="'. $value['id'] .'" data-toggle="modal" data-target="#duyetModal">Duyet</button>
              <button class="khongduyet btn btn-warning" data-id="'. $value['id'] .'">Khong duyet</button>';
        $xtpl->assign('IS_DUYET', $x);
    }else{
        $x ='<button class="unduyet btn btn-success" data-id="'. $value['id'] .'">Da Duyet</button>';
        $xtpl->assign('IS_DUYET', $x);
    }
    $xtpl->assign('VALUE', $value);
    $xtpl->assign('KEY', $key + 1);
    $xtpl->parse('main.list_du_an');

}

$xtpl->parse('main');
$contents = $xtpl->text('main');

include NV_ROOTDIR . '/includes/header.php';
echo nv_admin_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
