<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

$table_du_an = 'nv4_vi_duanquyengop_themmoiduans';

$fillable = [
    'ten_du_an',
    'mo_ta_ngan',
    'mo_ta_chi_tiet',
    'slug_du_an',
    'so_tien',
    'hinh_anh',
    'thoi_han',
    'is_open',
    'id_nguoi_tao',
];

$array_name = [
    'ten_du_an'         => 'Ten du an',
    'mo_ta_ngan'        => 'Mo ta ngan',
    'mo_ta_chi_tiet'    => 'Mo ta chi tiet',
    'hinh_anh'          => 'Hinh anh',
    'so_tien'           => 'Số tiền',
    'thoi_han'          => 'Thoi han',
    'is_open'           => 'Trang thai',
];

