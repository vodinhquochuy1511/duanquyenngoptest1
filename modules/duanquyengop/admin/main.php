<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_FILE_ADMIN')) {
    exit('Stop!!!');
}

include 'project.php';

global $user_info;

$page_title = $lang_module['main'];

if($nv_Request->isset_request('themmoi','post,get')){
    $request = request($_POST, $array_name);
    if($request){
        nv_jsonOutput([
            'status' => false,
            'error' => $request,
        ]);
    }else{
        $list_anh = '';
        $link_luu_anh = NV_ROOTDIR . '/' . NV_UPLOADS_DIR . '/' . $module_name;
        for ($i=0; $i < $_POST['so_luong_anh']; $i++) { 
            $upload = new NukeViet\Files\Upload('images', $global_config['forbid_extensions'], $global_config['forbid_mimes'], NV_UPLOAD_MAX_FILESIZE, 1600, 300);
            $upload->setLanguage($lang_global);
            $upload_info = $upload->save_file($_FILES['anh_' . $i] , $link_luu_anh, false, $global_config['nv_auto_resize']);
            if (strlen($list_anh) > 10) {
                $list_anh .= ',' . NV_BASE_SITEURL . '/' . NV_UPLOADS_DIR . '/' .$module_name . '/' . $upload_info['basename'];
            } else {
                $list_anh = NV_BASE_SITEURL . '/' . NV_UPLOADS_DIR . '/' .$module_name . '/' . $upload_info['basename'];
            }
        }

        $_POST['hinh_anh'] = $list_anh;

        foreach($fillable as $key => $value){
            $post[$value] = $_POST[$value];
        }
        $post['id_nguoi_tao'] = $admin_info['userid'];
        $post['is_duyet'] = 3;
        $post['slug_du_an'] = create_slug($_POST['ten_du_an']);
        $store =  store($post, $table_du_an);
        if($store){
            nv_jsonOutput([
                'status' => true,
            ]);
        }
    }
    
}

$xtpl = new XTemplate('main.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);

$xtpl->parse('main');
$contents = $xtpl->text('main');

include NV_ROOTDIR . '/includes/header.php';
echo nv_admin_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
